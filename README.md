# Preserves: an Expressive Data Language

This repository contains a [proposal](preserves.md) and
[various implementations](implementations/) of *Preserves*, a new data
model and serialization format in many ways comparable to JSON, XML,
S-expressions, CBOR, ASN.1 BER, and so on.

**WARNING** Everything in this repository is experimental and in flux!
The design of Preserves is not finalised and may change drastically.
Please offer any feedback you may have with this in mind.

## Contact

Tony Garnock-Jones <tonyg@leastfixedpoint.com>

## Licensing

The contents of this repository are made available to you under the
[Apache License, version 2.0](LICENSE)
(<http://www.apache.org/licenses/LICENSE-2.0>), and are Copyright
2018-2019 Tony Garnock-Jones.
