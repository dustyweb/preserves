pub mod value;

#[cfg(test)]
mod ieee754_section_5_10_total_order_tests {
    use std::cmp::Ordering::{Less, Equal, Greater};

    use crate::value::value::Value;
    // fn v<T>(val: T) -> Value where Value: std::convert::From<T> { Value::from(val) }
    fn f(val: f32) -> Value { Value::from(val) }
    fn d(val: f64) -> Value { Value::from(val) }

    // TODO: Test cases with a few different signalling and non-signalling NaNs

    #[test] fn case32_a_1() { assert_eq!(f(1.0).cmp(&f(2.0)), Less) }
    #[test] fn case32_a_2() { assert_eq!(f(-1.0).cmp(&f(1.0)), Less) }
    #[test] fn case32_a_3() { assert_eq!(f(0.0).cmp(&f(1.0)), Less) }
    #[test] fn case32_a_4() { assert_eq!(f(-1.0).cmp(&f(0.0)), Less) }
    #[test] fn case32_a_5() { assert_eq!(f(-1e32).cmp(&f(-1e31)), Less) }
    #[test] fn case32_a_6() { assert_eq!(f(-1e32).cmp(&f(1e33)), Less) }
    #[test] fn case32_a_7() {
        assert_eq!(f(std::f32::NEG_INFINITY).cmp(&f(std::f32::INFINITY)), Less)
    }
    #[test] fn case32_a_8() { assert_eq!(f(std::f32::NEG_INFINITY).cmp(&f(0.0)), Less) }
    #[test] fn case32_a_9() { assert_eq!(f(std::f32::NEG_INFINITY).cmp(&f(1.0)), Less) }
    #[test] fn case32_a_10() { assert_eq!(f(std::f32::NEG_INFINITY).cmp(&f(1e33)), Less) }
    #[test] fn case32_a_11() { assert_eq!(f(0.0).cmp(&f(std::f32::INFINITY)), Less) }
    #[test] fn case32_a_12() { assert_eq!(f(1.0).cmp(&f(std::f32::INFINITY)), Less) }
    #[test] fn case32_a_13() { assert_eq!(f(1e33).cmp(&f(std::f32::INFINITY)), Less) }

    #[test] fn case32_b_1() { assert_eq!(f(2.0).cmp(&f(1.0)), Greater) }
    #[test] fn case32_b_2() { assert_eq!(f(1.0).cmp(&f(-1.0)), Greater) }
    #[test] fn case32_b_3() { assert_eq!(f(1.0).cmp(&f(0.0)), Greater) }
    #[test] fn case32_b_4() { assert_eq!(f(0.0).cmp(&f(-1.0)), Greater) }
    #[test] fn case32_b_5() { assert_eq!(f(-1e31).cmp(&f(-1e32)), Greater) }
    #[test] fn case32_b_6() { assert_eq!(f(1e33).cmp(&f(-1e32)), Greater) }
    #[test] fn case32_b_7() {
        assert_eq!(f(std::f32::INFINITY).cmp(&f(std::f32::NEG_INFINITY)), Greater)
    }
    #[test] fn case32_b_8() { assert_eq!(f(std::f32::INFINITY).cmp(&f(0.0)), Greater) }
    #[test] fn case32_b_9() { assert_eq!(f(std::f32::INFINITY).cmp(&f(1.0)), Greater) }
    #[test] fn case32_b_10() { assert_eq!(f(std::f32::INFINITY).cmp(&f(1e33)), Greater) }
    #[test] fn case32_b_11() { assert_eq!(f(0.0).cmp(&f(std::f32::NEG_INFINITY)), Greater) }
    #[test] fn case32_b_12() { assert_eq!(f(1.0).cmp(&f(std::f32::NEG_INFINITY)), Greater) }
    #[test] fn case32_b_13() { assert_eq!(f(1e33).cmp(&f(std::f32::NEG_INFINITY)), Greater) }

    #[test] fn case32_c1() { assert_eq!(f(-0.0).cmp(&f( 0.0)), Less) }
    #[test] fn case32_c2() { assert_eq!(f( 0.0).cmp(&f(-0.0)), Greater) }
    #[test] fn case32_c3_1() { assert_eq!(f(-0.0).cmp(&f(-0.0)), Equal) }
    #[test] fn case32_c3_2() { assert_eq!(f( 0.0).cmp(&f( 0.0)), Equal) }
    #[test] fn case32_c3_3() { assert_eq!(f(1.0).cmp(&f(1.0)), Equal) }
    #[test] fn case32_c3_4() { assert_eq!(f(-1.0).cmp(&f(-1.0)), Equal) }
    #[test] fn case32_c3_5() { assert_eq!(f(-1e32).cmp(&f(-1e32)), Equal) }
    #[test] fn case32_c3_6() { assert_eq!(f(1e33).cmp(&f(1e33)), Equal) }


    #[test] fn case64_a_1() { assert_eq!(d(1.0).cmp(&d(2.0)), Less) }
    #[test] fn case64_a_2() { assert_eq!(d(-1.0).cmp(&d(1.0)), Less) }
    #[test] fn case64_a_3() { assert_eq!(d(0.0).cmp(&d(1.0)), Less) }
    #[test] fn case64_a_4() { assert_eq!(d(-1.0).cmp(&d(0.0)), Less) }
    #[test] fn case64_a_5() { assert_eq!(d(-1e32).cmp(&d(-1e31)), Less) }
    #[test] fn case64_a_6() { assert_eq!(d(-1e32).cmp(&d(1e33)), Less) }
    #[test] fn case64_a_7() {
        assert_eq!(d(std::f64::NEG_INFINITY).cmp(&d(std::f64::INFINITY)), Less)
    }
    #[test] fn case64_a_8() { assert_eq!(d(std::f64::NEG_INFINITY).cmp(&d(0.0)), Less) }
    #[test] fn case64_a_9() { assert_eq!(d(std::f64::NEG_INFINITY).cmp(&d(1.0)), Less) }
    #[test] fn case64_a_10() { assert_eq!(d(std::f64::NEG_INFINITY).cmp(&d(1e33)), Less) }
    #[test] fn case64_a_11() { assert_eq!(d(0.0).cmp(&d(std::f64::INFINITY)), Less) }
    #[test] fn case64_a_12() { assert_eq!(d(1.0).cmp(&d(std::f64::INFINITY)), Less) }
    #[test] fn case64_a_13() { assert_eq!(d(1e33).cmp(&d(std::f64::INFINITY)), Less) }

    #[test] fn case64_b_1() { assert_eq!(d(2.0).cmp(&d(1.0)), Greater) }
    #[test] fn case64_b_2() { assert_eq!(d(1.0).cmp(&d(-1.0)), Greater) }
    #[test] fn case64_b_3() { assert_eq!(d(1.0).cmp(&d(0.0)), Greater) }
    #[test] fn case64_b_4() { assert_eq!(d(0.0).cmp(&d(-1.0)), Greater) }
    #[test] fn case64_b_5() { assert_eq!(d(-1e31).cmp(&d(-1e32)), Greater) }
    #[test] fn case64_b_6() { assert_eq!(d(1e33).cmp(&d(-1e32)), Greater) }
    #[test] fn case64_b_7() {
        assert_eq!(d(std::f64::INFINITY).cmp(&d(std::f64::NEG_INFINITY)), Greater)
    }
    #[test] fn case64_b_8() { assert_eq!(d(std::f64::INFINITY).cmp(&d(0.0)), Greater) }
    #[test] fn case64_b_9() { assert_eq!(d(std::f64::INFINITY).cmp(&d(1.0)), Greater) }
    #[test] fn case64_b_10() { assert_eq!(d(std::f64::INFINITY).cmp(&d(1e33)), Greater) }
    #[test] fn case64_b_11() { assert_eq!(d(0.0).cmp(&d(std::f64::NEG_INFINITY)), Greater) }
    #[test] fn case64_b_12() { assert_eq!(d(1.0).cmp(&d(std::f64::NEG_INFINITY)), Greater) }
    #[test] fn case64_b_13() { assert_eq!(d(1e33).cmp(&d(std::f64::NEG_INFINITY)), Greater) }

    #[test] fn case64_c1() { assert_eq!(d(-0.0).cmp(&d( 0.0)), Less) }
    #[test] fn case64_c2() { assert_eq!(d( 0.0).cmp(&d(-0.0)), Greater) }
    #[test] fn case64_c3_1() { assert_eq!(d(-0.0).cmp(&d(-0.0)), Equal) }
    #[test] fn case64_c3_2() { assert_eq!(d( 0.0).cmp(&d( 0.0)), Equal) }
    #[test] fn case64_c3_3() { assert_eq!(d(1.0).cmp(&d(1.0)), Equal) }
    #[test] fn case64_c3_4() { assert_eq!(d(-1.0).cmp(&d(-1.0)), Equal) }
    #[test] fn case64_c3_5() { assert_eq!(d(-1e32).cmp(&d(-1e32)), Equal) }
    #[test] fn case64_c3_6() { assert_eq!(d(1e33).cmp(&d(1e33)), Equal) }
}
