"use strict";

if (require('./singletonmodule.js')('leastfixedpoint.com/preserves',
                                    require('../package.json').version,
                                    'index.js',
                                    module)) return;

Object.assign(module.exports, require('./symbols.js'));
Object.assign(module.exports, require('./codec.js'));
Object.assign(module.exports, require('./values.js'));
