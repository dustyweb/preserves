"use strict";

function initialize_singleton(namespace_key_str, package_version, module_key, module_object) {
  const namespace_key = Symbol.for(namespace_key_str);
  if (!(namespace_key in global)) {
    global[namespace_key] = {
      version: package_version,
      modules: {}
    };
  }
  let cache = global[namespace_key];
  if (cache.version !== package_version) {
    console.warn('Potentially incompatible versions of ' + namespace_key_str + ' loaded:',
                 cache.version,
                 package_version);
  }
  if (module_key in cache.modules) {
    module_object.exports = cache.modules[module_key];
    return true;
  } else {
    cache.modules[module_key] = module_object.exports;
    return false;
  }
}

module.exports = initialize_singleton;
