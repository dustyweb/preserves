"use strict";
// Symbols for various Preserves protocols.

if (require('./singletonmodule.js')('leastfixedpoint.com/preserves',
                                    require('../package.json').version,
                                    'symbols.js',
                                    module)) return;

const PreserveOn = Symbol.for('PreserveOn');
const AsPreserve = Symbol.for('AsPreserve');

Object.assign(module.exports, {
  PreserveOn,
  AsPreserve,
});
