"use strict";

const chai = require('chai');
const expect = chai.expect;
chai.use(require('chai-immutable'));

const Immutable = require('immutable');

const { is, Single, Double, fromJS } = require('../src/index.js');

describe('Single', () => {
  it('should print reasonably', () => {
    expect(Single(123.45).toString()).to.equal("123.45");
  });
});

describe('Double', () => {
  it('should print reasonably', () => {
    expect(Double(123.45).toString()).to.equal("123.45");
  });
});
