"use strict";
// We really, really, REALLY want not to load two separate
// implementations of values.js into the same node.js instance, so
// there's a bunch of singleton hackery in values.js. These tests
// check that separate loads don't cause separate instances.

const chai = require('chai');
const expect = chai.expect;
chai.use(require('chai-immutable'));

const Immutable = require('immutable');

describe('reloading values.js', () => {
  const V1 = require('../src/values.js');
  delete require.cache[require.resolve('../src/values.js')];
  const V2 = require('../src/values.js');

  const C1 = V1.Record.makeConstructor('c', ['a', 'b']);
  const C2 = V2.Record.makeConstructor('c', ['a', 'b']);

  it('should reuse RecordConstructorInfo (1)', () => {
    expect(C1.constructorInfo instanceof V1.RecordConstructorInfo).to.be.true;
  });
  it('should reuse RecordConstructorInfo (2)', () => {
    expect(C1.constructorInfo instanceof V2.RecordConstructorInfo).to.be.true;
  });
  it('should identify RecordConstructorInfo', () => {
    expect(Object.is(V1.RecordConstructorInfo, V2.RecordConstructorInfo)).to.be.true;
  });
  it('should produce identical module instances', () => { expect(V1 === V2).to.be.true; });

  it('should produce distinct constructor instances', () => { expect(C1 === C2).to.be.false; });
  it('should produce distinct constructor info', () => {
    expect(Object.is(C1.constructorInfo, C2.constructorInfo)).to.be.false;
  });
  it('should produce compatible constructor info', () => {
    expect(Immutable.is(C1.constructorInfo, C2.constructorInfo)).to.be.true;
  });
  it('should produce compatible record instances', () => {
    expect(Immutable.is(C1(1,2), C2(1,2))).to.be.true;
  });
});

describe('reloading index.js', () => {
  it('produces identical module exports objects', () => {
    const I1 = require('../src/index.js');
    delete require.cache[require.resolve('../src/index.js')];
    const I2 = require('../src/index.js');
    expect(Object.is(I1, I2)).to.be.true;
  });
});
