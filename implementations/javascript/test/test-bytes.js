"use strict";

const chai = require('chai');
const expect = chai.expect;
chai.use(require('chai-immutable'));

const Immutable = require('immutable');

const { is, Bytes, fromJS } = require('../src/index.js');

describe('immutable byte arrays', () => {
  describe('Uint8Array methods', () => {
    const bs = Bytes.of(10, 20, 30, 40);
    it('should yield entries', () => {
      expect(fromJS(Array.from(bs.entries()))).to.equal(fromJS([[0,10],[1,20],[2,30],[3,40]]));
    });
    it('should implement every', () => {
      expect(bs.every((b) => !(b & 1))).to.be.true;
      expect(bs.every((b) => b !== 50)).to.be.true;
      expect(bs.every((b) => b !== 20)).to.be.false;
    });
    it('should implement find', () => {
      expect(bs.find((b) => b > 20)).to.equal(30);
      expect(bs.find((b) => b > 50)).to.be.undefined;
    });
    it('should implement findIndex', () => {
      expect(bs.findIndex((b) => b > 20)).to.equal(2);
      expect(bs.findIndex((b) => b > 50)).to.equal(-1);
    });
    it('should implement forEach', () => {
      const vs = [];
      bs.forEach((b) => vs.push(b));
      expect(fromJS(vs)).to.equal(fromJS([10, 20, 30, 40]));
    });
    it('should implement includes', () => {
      expect(bs.includes(20)).to.be.true;
      expect(bs.includes(50)).to.be.false;
    });
    it('should implement indexOf', () => {
      expect(bs.indexOf(20)).to.equal(1);
      expect(bs.indexOf(50)).to.equal(-1);
    });
    it('should implement join', () => expect(bs.join('-')).to.equal('10-20-30-40'));
    it('should implement keys', () => {
      expect(fromJS(Array.from(bs.keys()))).to.equal(fromJS([0,1,2,3]));
    });
    it('should implement values', () => {
      expect(fromJS(Array.from(bs.values()))).to.equal(fromJS([10,20,30,40]));
    });
    it('should implement filter', () => {
      expect(is(bs.filter((b) => b !== 30), Bytes.of(10,20,40))).to.be.true;
    });
    it('should implement slice', () => {
      const vs = bs.slice(2);
      expect(Object.is(vs._view.buffer, bs._view.buffer)).to.be.false;
      expect(vs._view.buffer.byteLength).to.equal(2);
      expect(vs.get(0)).to.equal(30);
      expect(vs.get(1)).to.equal(40);
      expect(vs.size).to.equal(2);
    });
    it('should implement subarray', () => {
      const vs = bs.subarray(2);
      expect(Object.is(vs._view.buffer, bs._view.buffer)).to.be.true;
      expect(vs._view.buffer.byteLength).to.equal(4);
      expect(vs.get(0)).to.equal(30);
      expect(vs.get(1)).to.equal(40);
      expect(vs.size).to.equal(2);
    });
    it('should implement reverse', () => {
      const vs = bs.reverse();
      expect(Object.is(vs._view.buffer, bs._view.buffer)).to.be.false;
      expect(bs.get(0)).to.equal(10);
      expect(bs.get(3)).to.equal(40);
      expect(vs.get(0)).to.equal(40);
      expect(vs.get(3)).to.equal(10);
    });
    it('should implement sort', () => {
      const vs = bs.reverse().sort();
      expect(Object.is(vs._view.buffer, bs._view.buffer)).to.be.false;
      expect(bs.get(0)).to.equal(10);
      expect(bs.get(3)).to.equal(40);
      expect(vs.get(0)).to.equal(10);
      expect(vs.get(3)).to.equal(40);
    });
  });
});
